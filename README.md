# P1 cookie telegram parser

Service for listening on the MQTT bus for P1 cookie (digital utility meter) messages, parsing the contained metering telegram, processing it and sending it to the standardised COFY MQTT endpoints.
If requested, the service also registers the device with HA.
Configuration is done through configuration files which must be placed in the same directory as this file.
This code is meant to be run continously as a Unix service. See the dryrun version for testing.

## Manual installation as service
1. Clone or import this repository to an executable location (e.g. user home directory)
2. In `p1telegramparse.service` under `[Service]`:
    * Change `User` to your user
    * Check location of python3
    * Change location of `p1telegramparse.py`
3. Move `p1telegramparse.service` to `/etc/systemd/`
    >sudo mv p1telegramparse.service /etc/systemd/p1telegramparse.service
4. Start the service
    >sudo systemctl daemon-reload
    
    >sudo systemctl start p1telegramparse.service
5. (Optional) Make the service persistent
    >sudo systemctl enable p1telegramparse.service
6. (Optional) Check if the service is running
    >sudo systemctl status p1telegramparse.service

## Simple test run
`python3 p1telegramparse_dryrun.py` or `python p1telegramparse_dryrun.py`, depending on your Python installation, will execute the parser once. `p1telegram.txt` contains a sample MQTT message from the P1 cookie. Setting the `_PRODUCTION_`variable to `True` enables a MQTT push of the processed data.

## Requirements
* Standard Python 3.7 or newer installation
* paho-mqtt 1.5.1 or newer https://pypi.org/project/paho-mqtt/
* PyYAML 5.3.1 or newer https://pypi.org/project/PyYAML/


Additionally, ensure the system locale is set correctly.

Tested on Raspbian GNU/Linux 10 (buster) with Python 3.7.3
